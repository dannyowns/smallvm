module DrawBot
author 'MicroBlocks and José García'
version 1 1 
depends Servo 
tags cnc protoneer drawbot plotter 
description 'Control CoreXY DrawBots that use the Protoneer CNC shield.'
variables _plotterDelay _plotterHeading

	spec ' ' '_plotter init' '_plotter init'
	spec ' ' '_plotter step A' '_plotter step A'
	spec ' ' '_plotter step B' '_plotter step B'
	spec ' ' 'plotter move' 'plotter move X _ Y _' 'auto auto' 1000 2000
	spec ' ' 'ploturtle move' 'plotter move _ steps' 'auto' '1000'
	spec ' ' 'ploturtle turnBy' 'plotter turn _ degrees' 'auto' 90
	spec ' ' 'ploturtle turnFraction' 'plotter turn _ / _ of circle' 'auto auto' 1 4
	spec ' ' 'ploturtle setHeading' 'plotter point in direction _' 'auto' 0
	spec ' ' 'plotter pen down' 'plotter pen down'
	spec ' ' 'plotter pen up' 'plotter pen up'
	spec ' ' 'plotter stop' 'plotter stop'
	spec ' ' 'set plotter speed to' 'set plotter speed to _ %' 'auto' 10

to '_plotter init' {
  if (_plotterDelay == 0) {'set plotter speed to' 10}
  comment 'enable steppers'
  digitalWriteOp 8 false
}

to '_plotter step A' {
  digitalWriteOp 2 true
  waitMicros _plotterDelay
  digitalWriteOp 2 false
}

to '_plotter step B' {
  digitalWriteOp 3 true
  waitMicros _plotterDelay
  digitalWriteOp 3 false
}

to 'plotter move' x y {
  '_plotter resolution' = 10000
  '_plotter init'
  local 'delta A' (x + y)
  local 'delta B' (x - y)
  digitalWriteOp 5 ((v 'delta A') >= 0)
  digitalWriteOp 6 ((v 'delta B') >= 0)
  motorA = ('[data:makeList]')
  motorB = ('[data:makeList]')
  local 'relation' 0
  if (and ((v 'delta A') != 0) ((v 'delta B') != 0)) {
    if ((absoluteValue (v 'delta A')) >= (absoluteValue (v 'delta B'))) {
      relation = (absoluteValue (((v 'delta B') * (v '_plotter resolution')) / (v 'delta A')))
      local 'r' 0
      repeat (absoluteValue (v 'delta A')) {
        r += relation
        if (r >= (v '_plotter resolution')) {
          r += (0 - (v '_plotter resolution'))
          '_plotter step A'
          '_plotter step B'
        } else {
          '_plotter step A'
        }
      }
    } else {
      relation = (absoluteValue (((v 'delta A') * (v '_plotter resolution')) / (v 'delta B')))
      local 'r' 0
      repeat (absoluteValue (v 'delta B')) {
        r += relation
        if (r >= (v '_plotter resolution')) {
          r += (0 - (v '_plotter resolution'))
          '_plotter step A'
          '_plotter step B'
        } else {
          '_plotter step B'
        }
      }
    }
  } else {
    if ((v 'delta B') == 0) {
      repeat (absoluteValue (v 'delta A')) {
        '_plotter step A'
      }
    } ((v 'delta A') == 0) {
      repeat (absoluteValue (v 'delta B')) {
        '_plotter step B'
      }
    }
  }
}

to 'plotter pen down' {
  setServoAngle 11 0
  waitMillis 250
}

to 'plotter pen up' {
  setServoAngle 11 -90
}

to 'plotter stop' {
  stopAll
  comment 'disable steppers'
  digitalWriteOp 8 true
  'plotter pen up'
}

to 'set plotter speed to' speed {
  _plotterDelay = ((950 / (maximum 1 (minimum 100 speed))) + 41)
}

to 'ploturtle move' n {
  'plotter move' ((n * ('[misc:sin]' (_plotterHeading + 9000))) >> 14) ((n * ('[misc:sin]' _plotterHeading)) >> 14)
}

to 'ploturtle setHeading' a {
  _plotterHeading = ((a * 100) % 36000)
}

to 'ploturtle turnBy' a {
  _plotterHeading += (a * 100)
  _plotterHeading = (_plotterHeading % 36000)
}

to 'ploturtle turnFraction' num denom {
  _plotterHeading += ((num * 36000) / denom)
  _plotterHeading = (_plotterHeading % 36000)
}

